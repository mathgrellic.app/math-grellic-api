export enum ContentStatus {
  Draft = 'draft',
  Published = 'published',
}
